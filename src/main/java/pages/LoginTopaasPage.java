package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.Testbase;

public class LoginTopaasPage extends Testbase {

    private WebDriver driver;

    public LoginTopaasPage(WebDriver driver) {
           this.driver = driver;
    }

     private By usernameField = By.name("username");
     private By passwordField = By.name("password");
     private By sumbitButton = By.xpath("//input[@class='credentials_input_submit']");

     public ServiceDeskPage loginWithCredentials(String username, String password) {
         driver.findElement(usernameField).sendKeys(username);
         driver.findElement(passwordField).sendKeys(password);
         driver.findElement(sumbitButton).click();
         clickElement(driver, usernameField); // variant op driver.findelement
         return new ServiceDeskPage(driver);
     }
}
