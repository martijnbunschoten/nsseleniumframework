package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.Testbase;

public class ServiceDeskPage extends Testbase {

    private WebDriver driver;

    public ServiceDeskPage(WebDriver driver) {
        this.driver = driver;
    }

    private By aanvraagTopaasOmgeving = By.xpath("//a//strong[contains(text(), 'Aanvraag TOPAAS')]");

    public RequestFormPage clickOnAanVragenTopaasOmgeving() throws InterruptedException {
           Thread.sleep(3000);
           driver.findElement(aanvraagTopaasOmgeving).click();
           return new RequestFormPage(driver);
    }
}
