package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RequestFormPage {

    private WebDriver driver;

    public RequestFormPage(WebDriver driver) {
        this.driver = driver;
    }

    private By omgevingsnaamField = By.id("customfield_21200");
    private By omgevingsbeschrijvingField = By.id("description");
    private By aanvullendeOpmerkingenField = By.id("customfield_19501");
    private By createButton = By.className("aui-button aui-button-primary");

    public RequestFormPage enterOmgevingsnaam(String omgevingsnaam) {
        driver.findElement(omgevingsnaamField).sendKeys(omgevingsnaam);
        return this;
    }

    public RequestFormPage enterOmgevingsBeschrijving(String beschrijving) {
        driver.findElement(omgevingsbeschrijvingField).sendKeys(beschrijving);
        return this;
    }

    public RequestFormPage enterAanvullendeOpmerkingen(String opmerkingen) {
        driver.findElement(aanvullendeOpmerkingenField).sendKeys(opmerkingen);
        return this;
    }

    public RequestFormPage clickOnCreate() {
        driver.findElement(createButton).click();
        return this;
    }
}
