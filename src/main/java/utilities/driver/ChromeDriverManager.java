package utilities.driver;

import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class ChromeDriverManager extends DriverManager {

  @Override
    public void createWebDriver() {
      System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
      this.driver = new ChromeDriver();
      driver.manage().window().maximize();
      driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
      driver.manage().deleteAllCookies();
    }
}
