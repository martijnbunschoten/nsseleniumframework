package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyManager {

   private static Properties prop;

    public static String getProperty(String property) {

        try (InputStream input = new FileInputStream("src\\main\\resources\\config.properties")) {
            prop = new Properties();
            prop.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return prop.getProperty(property);
    }
}
