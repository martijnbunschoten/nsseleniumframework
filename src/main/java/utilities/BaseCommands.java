package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BaseCommands {

    public void clickElement(WebDriver driver, By element) {
        driver.findElement(element).click();
    }

    public void enterText(WebDriver driver, By element, String text) {
        driver.findElement(element).sendKeys(text);
    }
}
