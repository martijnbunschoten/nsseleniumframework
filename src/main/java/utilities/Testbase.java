package utilities;

import jenkins.TwoFactorAuth;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.LoginTopaasPage;
import utilities.driver.DriverManager;
import utilities.driver.DriverManagerFactory;
import utilities.driver.DriverType;

public class Testbase extends BaseCommands {

    private DriverManager driverManager;
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
        driver = driverManager.getwebriver();
    }

    @AfterClass
    public void tearDown() {
        driverManager.quitWebDriver();
     }

    public LoginTopaasPage navigateToUrl(String url) {
        driver.get(url);
        return new LoginTopaasPage(driver);
    }

    public TwoFactorAuth navigateToJenkins(String url) {
        driver.get(url);
        return new TwoFactorAuth(driver);
    }
}
