package jenkins;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.PropertyManager;
import utilities.Testbase;
import java.io.IOException;

public class TwoFactorAuth extends Testbase {

    private WebDriver driver;
    private String smsCode;

  public TwoFactorAuth(WebDriver driver) {
      this.driver = driver;
  }

  private By signInFieldMicrosoft = By.id("i0116");
  private By nextButtonMicroSoft = By.id("idSIButton9");
  private By passwordFieldNs = By.id("passwordInput");
  private By signInFieldNs = By.id("submitButton");
  private By smsCodeField = By.id("otp");
  private By signInFieldSms = By.id("continueButton");

  public TwoFactorAuth loginWithCredentials(String email, String password) throws InterruptedException {
         // enterText(driver, signInFieldMicrosoft, email);
        driver.findElement(signInFieldMicrosoft).sendKeys(email);
        driver.findElement(nextButtonMicroSoft).click();

        Thread.sleep(3000);
        driver.findElement(passwordFieldNs).sendKeys(password);
        driver.findElement(signInFieldNs).click();
        return this;
  }

  // werkt, moet alleen nog getest worden met ns account
  public TwoFactorAuth completeLoginWithSmsCode() throws IOException, InterruptedException {
      driver.findElement(signInFieldSms).click(); // first trigger the sms being send

      try {
          Response response = RestAssured.given()
                  .when()
                  .get(PropertyManager.getProperty("sms_service"))
                  .then().extract().response();
          smsCode = response.getBody().asString();
      } catch (Exception e) {
          e.printStackTrace();
      }

      System.out.println(smsCode);

      driver.findElement(smsCodeField).sendKeys(smsCode);
      driver.findElement(signInFieldSms).click();

      return this;
  }

  public TwoFactorAuth getJenkinsVersion() {
      return this;
    }
}
