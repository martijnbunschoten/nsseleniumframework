import org.testng.annotations.Test;
import utilities.PropertyManager;
import utilities.Testbase;
import java.io.IOException;

public class TwoFactorAuthTest extends Testbase {

    @Test
      public void pingJenkinsTest() throws IOException, InterruptedException {
          navigateToJenkins(PropertyManager.getProperty("ts_jenkins"))
          .loginWithCredentials(PropertyManager.getProperty("mail"), PropertyManager.getProperty("password"))
          .completeLoginWithSmsCode();
      }
}
