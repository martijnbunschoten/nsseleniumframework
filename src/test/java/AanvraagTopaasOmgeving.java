import org.testng.annotations.Test;
import utilities.PropertyManager;
import utilities.Testbase;

public class AanvraagTopaasOmgeving extends Testbase {

    @Test
    public void topaasOmgevingAanvragenTest() throws InterruptedException {

         navigateToUrl(PropertyManager.getProperty("topaasServiceDesk"))
        .loginWithCredentials(PropertyManager.getProperty("username"), PropertyManager.getProperty("password"))
        .clickOnAanVragenTopaasOmgeving()
        .enterOmgevingsnaam("TEST")
        .enterOmgevingsBeschrijving("Dit is een test")
        .enterAanvullendeOpmerkingen("Wederom een test");
    }
}
